import protoModel from '../protobuf_compiled/me/seroperson/gate/forhonor/model_pb.js';
import * as shared from '../shared.js';

const entries = [
  {
    title: "11 jan 2021",
    content: `
      <li>splitting by seasons 15 and 16.</li>
      <li>recognizing <img class="hero-avatar" alt="` + shared.heroIdToShortName[protoModel.Hero.GRYPHON] + `" src="` + shared.heroIdToImagePath(protoModel.Hero.GRYPHON) + `" />'s changes.</li>
    `
  },
  {
    title: "31 august 2020",
    content: `
      <li>implementing overall per-season stats (available per platform at <a href="/pc/player/" target="_blank">players</a> page).</li>
      <li>splitting season 14 by pre and post ccu.</li>
      <li>recognizing <img class="hero-avatar" alt="` + shared.heroIdToShortName[protoModel.Hero.WARMONGER] + `" src="` + shared.heroIdToImagePath(protoModel.Hero.WARMONGER) + `" />'s changes.</li>
    `
  },
  {
    title: "19 april 2020",
    content: `
      <li>improving navigation by placing all links on top of page.</li>
      <li>cleaning main-page, separating it into several pages: <a href="/faq" target="_blank">faq</a>, <a href="/changelog" target="_blank">changelog</a>, <a href="/contacts" target="_blank">contacts</a>.</li>
      <li>fixing incorrectly displayed icons when browsing with safari.</li>
      <li>introducing <a href="/pc/clan/" target="_blank">clan</a> pages.</li>
    `
  },
  {
    title: "23 february 2020",
    content: `
      <li>mastering profile page ui:
      <ul>
        <li>improving page header to make it more compact.</li>
        <li>reorganizing 'recent changes' list so it looks better on phones.</li>
        <li>grouping 'recent changes' elements by days.</li>
        <li>improving 'per-mode statistics' table ux expirience.</li>
      </ul>
      </li>
    `
  },
  {
    title: "18 february 2020",
    content: `
      <li>implementing sane <a href="https://ogp.me/" target="_blank">open graph</a> support.</i>
      <li>match history changes ('recent changes' section at player's profile page):
      <ul>
        <li>now contains results across all matchmaking modes (public, custom, private etc). some inaccuracies are possible with ubisoft's data decoding, so pm me if you noticed something wrong.</li>
        <li>now logs also bot/minion kills.</li>
        <li>kills/assists were merged into single value.</li>
      </ul>
      </li>
      <li>added splitting for season 13.</li>
      <li>minor front-end/back-end tweaks. better queue handling now allows us to painless request more players' data at time.</li>
    `
  },
  {
    title: "20 january 2020",
    content: `
      <li>watching for player's actual nicknames. <i>previously the nickname was fetched just once in moment when player being tracked.</i>
      <ul>
        <li>some obliged routing tweaks, so now you can access player's page by its' permanent profile id. old logic was perserved, so accessing by nickname still works too. for example: link <a href="pc/player/seroperson">by name</a> and <a href="pc/player/5a726ed0-8766-458e-a9d0-03f368737175">by profile id</a>.</li>
      </ul>
      </li>
      <li>rethinking and improving 'per-mode statistics' section at player's page. now you can investigate most-played modes as well as observe some additional overall information.</li>
      <li>page '<a href="pc/player/">all currently tracked players</a>' now sorted by player's activity.</li>
      <li>minor front-end/back-end tweaks.</li>
    `
  },
  {
    title: "01 december 2019",
    content: `
      <li>recent ubisoft's technical maintenance caused some problems for me, so several internal optimizations were implemented in last few days. seem to be ready for such situations now.</li>
    `
  },
  {
    title: "12 november 2019",
    content: `
      <li>added splitting by season 12 for pc platform.</li>
      <li>recognizing <img class="hero-avatar" alt="` + shared.heroIdToShortName[protoModel.Hero.ZHANHU] + `" src="` + shared.heroIdToImagePath(protoModel.Hero.ZHANHU) + `" />'s changes.</li>
      <li>supporting xbox, psn platforms - now you can notice different urls for each platform. these platforms start their history from current season.</li>
    `
  },
  {
    title: "05 november 2019",
    content: `
      <li>due to technical issues, some data (leaderboard iterations, stat diffs and newly tracked players) between <b>25 october</b> and <b>05 november</b> were lost. as consequences of this incident, you can notice large diffs in player's game history. however, all changes are counted in right season, so it's not affecting your future overall history. sorry for this fault - i have to be careful in the future.</li>
    `
  },
  {
    title: "26 october 2019",
    content: `
      <li>batch of <a href="/leaderboard">leaderboard page</a> changes:
      <ul>
        <li>the page was splitted into 'activity' and 'history' sub-pages.</li>
        <li>be able to browse past leaderboard states (up to season 10 for pc).</li>
        <li>displaying player's position changes according to previous leaderboard snapshot.</li>
        <li>'recent activity' sub-page now has information about player's current leaderboard position and their most-played ranked hero.</li>
      </ul>
      </li>
      <li>player's statistics page (ex <a href="/player/seroperson">link</a>) now has 'total' line and kd/wl values.</li>
    `
  },
  {
    title: "06 october 2019",
    content: `
      <li><a href="https://www.reddit.com/r/CompetitiveForHonor/comments/de5272/introducing_one_more_stats_assistant/" target="_blank">initial release</a>.</li>
    `
  }
]

export default entries;
