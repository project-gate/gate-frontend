import proto from '../../../../protobuf_server_compiled/me/seroperson/gate/forhonor/og_service_grpc_pb.js';
import protoServiceModel from '../../../../protobuf_server_compiled/me/seroperson/gate/forhonor/og_service_pb.js';
import grpc from 'grpc'
import * as shared from '../../../../shared.js';
import * as sharedServer from '../../../../shared_server.js';

export async function get(req, res, next) {
  const { platform, name } = req.params;

  var client = sharedServer.createOgService(platform)

  var request = new protoServiceModel.OgClanRequest();
  request.setId(name);

  var metadata = new grpc.Metadata();
  metadata.set("platform", platform);

  var response = new Promise((resolve, reject) => {
    client.getClan(request, metadata, (error, response) => {
      if (error) {
        reject(error);
      }
      resolve(response);
    });
  })
    .then(function(r) {
      return r.toObject()
    })
    .catch(function(e) {
      return e
    })

  var result = await response
  var jsonResult = JSON.stringify(result)

  res.end(jsonResult);
}
