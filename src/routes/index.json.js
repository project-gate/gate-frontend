import entries from './_changelog.js';

const contents = JSON.stringify(
  entries.map(post => {
    return {
      title: post.title,
      content: post.content
    };
  })
)

export function get(req, res) {
  res.writeHead(200, {
    'Content-Type': 'application/json'
  });
  res.end(contents);
}
