import proto from './protobuf_compiled/me/seroperson/gate/forhonor/main_service_grpc_web_pb.js';
import protoModel from './protobuf_compiled/me/seroperson/gate/forhonor/model_pb.js';

export const dev = process.env.NODE_ENV === 'development';

export let definedPlatforms = [
  "pc",
  "xbox",
  "psn"
]

export function createMainService(platform) {
  if(dev) {
    return new proto.MainServicePromiseClient("http://localhost:9001/" + platform);
  } else {
    return new proto.MainServicePromiseClient("https://gate.seroperson.me:9000/" + platform);
  }
}

export let qualifierIdToShortName = {
  0: "public",
  1: "public",
  2: "private",
  3: "custom",
  4: "practice",
  5: "?"
};

export let heroIdToShortName = {
  1: "warden",
  2: "pk",
  3: "cent",
  4: "glad",
  5: "lb",
  6: "conq",
  7: "orochi",
  8: "kensei",
  9: "nobu",
  10: "shino",
  11: "musha",
  12: "shugo",
  13: "zerk",
  14: "raider",
  15: "hl",
  16: "shaman",
  17: "valk",
  18: "wl",
  19: "nuxia",
  20: "tiandi",
  21: "jj",
  22: "shao",
  23: "bp",
  24: "sakura",
  25: "jorm",
  26: "zhanhu",
  27: "warmonger",
  28: "gryphon"
};

export let heroIdToImage = {
  1: "warden",
  2: "peacekeeper",
  3: "centurion",
  4: "gladiator",
  5: "lawbringer",
  6: "conqueror",
  7: "orochi",
  8: "kensei",
  9: "nobushi",
  10: "shinobi",
  11: "aramusha",
  12: "shugoki",
  13: "berserker",
  14: "raider",
  15: "highlander",
  16: "shaman",
  17: "valkyrie",
  18: "warlord",
  19: "nuxia",
  20: "tiandi",
  21: "jj",
  22: "shaolin",
  23: "blackprior",
  24: "sakura",
  25: "jormungand",
  26: "zhanhu",
  27: "warmonger",
  28: "gryphon"
};

export let gameToShortName = {
  1: "dominion",
  2: "brawl",
  3: "duel",
  4: "deathmatch",
  5: "last man standing",
  6: "breach",
  7: "tribute",
  8: "campaign",
  9: "arcade",
  10: "training",
  11: "ranked duel"
};

export function heroIdToImagePath(heroId, extension = "webp") {
  return "/icon_hero_" + heroIdToImage[heroId] + "." + extension;
}

export function timestamp2string(timestamp) {
  return new Date(timestamp).toLocaleDateString("en-US", {
    day : 'numeric',
    month : 'short',
    year : 'numeric'
  })
}

export function timeDifference(current, previous) {
  var msPerMinute = 60 * 1000;
  var msPerHour = msPerMinute * 60;
  var msPerDay = msPerHour * 24;
  var msPerMonth = msPerDay * 30;
  var msPerYear = msPerDay * 365;
  var elapsed = current - previous;
  if (elapsed < msPerMinute) {
    return Math.round(elapsed/1000) + " seconds";
  } else if (elapsed < msPerHour) {
    return Math.round(elapsed/msPerMinute) + " minutes";
  } else if (elapsed < msPerDay) {
    return Math.round(elapsed/msPerHour ) + " hours";
  } else if (elapsed < msPerMonth) {
    return Math.round(elapsed/msPerDay) + " days";
  } else if (elapsed < msPerYear) {
    return Math.round(elapsed/msPerMonth) + " months";
  } else {
    return Math.round(elapsed/msPerYear ) + " years";
  }
}

export function findKey(where, value) {
  return Object.keys(where).find(key => where[key] === value);
}

export function parsePlayerId(str) {
  var result = new protoModel.PlayerIdentity()
  var profileIdPattern = /^\w{8}-\w{4}-\w{4}-\w{4}-\w{12}$/
  var profileIdExec = profileIdPattern.exec(str)
  if(profileIdExec) {
    result.setProfileid(str)
  } else {
    result.setName(str)
  }
  return result
}

export function clanCountry(clan) {
  if(clan.country == "ru") {
    return "🇷🇺"
  }
  return clan.country
}

export let wait = ms => new Promise((r, j) => setTimeout(r, ms));
