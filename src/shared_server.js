import proto from './protobuf_server_compiled/me/seroperson/gate/forhonor/og_service_grpc_pb.js';
import protoModel from './protobuf_server_compiled/me/seroperson/gate/forhonor/model_pb.js';
import grpc from 'grpc'
import * as shared from './shared.js'

export function createOgService(platform) {
  if(shared.dev) {
    return new proto.OgServiceClient("localhost:9000", grpc.credentials.createInsecure());
  } else {
    return new proto.OgServiceClient("172.18.0.2:6000", grpc.credentials.createInsecure());
  }
}

export function parsePlayerId(str) {
  var result = new protoModel.PlayerIdentity()
  var profileIdPattern = /^\w{8}-\w{4}-\w{4}-\w{4}-\w{12}$/
  var profileIdExec = profileIdPattern.exec(str)
  if(profileIdExec) {
    result.setProfileid(str)
  } else {
    result.setName(str)
  }
  return result
}
