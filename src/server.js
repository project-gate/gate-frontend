import sirv from 'sirv';
import polka from 'polka';
import redirect from '@polka/redirect';
import compression from 'compression';
import * as sapper from '@sapper/server';
import * as shared from './shared.js';

const { PORT, NODE_ENV } = process.env;
const dev = NODE_ENV === 'development';

global.XMLHttpRequest = require("xmlhttprequest").XMLHttpRequest;

const isBotRegExp = /facebookexternalhit|Twitterbot|vkShare|WhatsApp|TelegramBot|Slack|OdklBot|Discord/

function detectBot(req, res, next) {
  let ua = req.headers["user-agent"]
  if (!ua) {
    req.isBot = false
    next();
  } else {
    req.isBot = isBotRegExp.test(ua)
    next()
  }
}

function redirectBot(req, res, next) {
  if(req.isBot) {
    req.url = req.path = req.originalUrl = "/og" + req.url
  }
  next()
}

polka()
  .use(detectBot, redirectBot)
  .get("/leaderboard", (req, res) => {
    redirect(res, "/pc/leaderboard")
  })
  .get("/player/:name", (req, res) => {
    let { name } = req.params;
    redirect(res, "/pc/player/" + name)
  })
  .get("/player", (req, res) => {
    redirect(res, "/pc/player")
  })
  .use(
    compression({ threshold: 0 }),
    sirv('static', { maxAge: 31536000, immutable: true }),
    sapper.middleware({
      session: (req, res) => ({
        isBot: req.isBot
      })
    })
  )
  .listen(PORT, err => {
    if (err) {
      console.log('error', err);
    }
  });
